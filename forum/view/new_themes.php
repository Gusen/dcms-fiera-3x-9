<?php

$set['title'] = 'Новые темы';
include_once '../sys/inc/thead.php';
title().aut();

$k_post = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_themes` WHERE `time` > '.(time()-60*60*24)), 0);
$k_page = k_page($k_post, $set['p_str']);
$page = page($k_page);
$start = $set['p_str']*$page-$set['p_str'];
if ($k_post == 0) {
    ?>
    <div class = 'err'>За последние 24 часа не было создано ни одной темы.</div>
    <?
} else {
    ?>
    <table class = 'post'>
        <?
        $themes = mysql_query('SELECT * FROM `forum_themes` WHERE `time` > '.(time()-60*60*24).' ORDER BY `id` DESC LIMIT '.$start.', '.$set['p_str']);
        while ($theme = mysql_fetch_object($themes)) {
            $creater = mysql_fetch_object(mysql_query('SELECT `id`, `nick` FROM `user` WHERE `id` = '.$theme->id_user));
            $razdel = mysql_fetch_object(mysql_query('SELECT `id`, `id_forum` FROM `forum_razdels` WHERE `id` = '.$theme->id_razdel));
            $forum = mysql_fetch_object(mysql_query('SELECT `id` FROM `forum` WHERE `id` = '.$razdel->id_forum));
            $hide = (user_access('forum_post_ed')) ? NULL : '`hide` = "0" AND';
            $last_post = mysql_fetch_object(mysql_query('SELECT `id`, `hide`, `id_user`, `time` FROM `forum_posts` WHERE '.$hide.' `id_theme` = '.$theme->id.' ORDER BY `id` DESC'));
            
            $who = ($last_post && $last_post->id_user != 0) ? mysql_fetch_object(mysql_query('SELECT `id`, `nick` FROM `user` WHERE `id` = '.$last_post->id_user)) : array();
            $who_id = ($last_post && $last_post->id_user != 0) ? $who->id : 0;
            $who_nick = ($last_post && $last_post->id_user != 0) ? $who->nick : 'Система';
            
            ?>
            <tr>
                <td class = 'icon14'>
                    <img src = '<?= FORUM ?>/icons/theme.png' alt = '' <?= ICONS ?> />
                </td>
                <td class = 'p_t'>
                    <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?= output_text($theme->name, 1, 1, 0, 0, 0) ?></a> (<?= vremja($theme->time) ?>)
                </td>
            </tr>
            <tr>
                <?
                if ($last_post) {
                    ?>
                    <td class = 'p_m' colspan = '2'>
                        <b>Автор: <a href = '/info.php?id=<?= $theme->id_user ?>'><?= $creater->nick ?></a> | Посл.:</b> <a href = '/info.php?id=<?= $who_id ?>'><?= $who_nick ?></a> (<?= vremja($last_post->time) ?>)
                    </td>
                    <?
                } else {
                    $continue = (mb_strlen($theme->description) > 150) ? '.....' : NULL;
                    ?>
                    <td class = 'p_m' colspan = '2'>
                        <?= output_text(mb_substr($theme->description, 0, 150), 1, 1, 0, 1, 1).$continue ?>
                    </td>
                    <?
                }
                ?>
            </tr>
            <?
        }
        ?>
    </table>
    <?
    if ($k_page > 1) {
        str(FORUM.'/new_themes/', $k_page, $page);
    }
}

?>
<div class = 'p_m' style = 'text-align: right'><a href = '<?= FORUM ?>'>Вернуться в форум</a></div>