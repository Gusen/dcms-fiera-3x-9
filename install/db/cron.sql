-- phpMyAdmin SQL Dump
-- version 4.3.4
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июл 03 2015 г., 03:32
-- Версия сервера: 5.6.22-log
-- Версия PHP: 5.5.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `test_local`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cron`
--

CREATE TABLE IF NOT EXISTS `cron` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `time` int(11) NOT NULL,
  `name` varchar(64) DEFAULT '',
  `opis` varchar(999) DEFAULT '',
  `file` varchar(99) DEFAULT '',
  `type` varchar(32) DEFAULT '',
  `act` int(11) DEFAULT '1',
  `system` int(11) DEFAULT '0',
  `time_update` int(11) DEFAULT '0',
  `count` int(11) DEFAULT '0',
  KEY `time` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cron`
--

INSERT DELAYED INTO `cron` (`id`, `time`, `name`, `opis`, `file`, `type`, `act`, `system`, `time_update`, `count`) VALUES
(1, 1435978783, 'Создание бекапа базы данных', 'Процесс создает бекап базы данных и отправляется на почту указанную в настройках', 'backup.php', '', 1, 0, 86400, 33),
(2, 1435892419, 'Отправка запланированных писем', 'Задача которая рассылает сообщение на постовые ящики так что бы ваш сайт,сервер,ip .как можно менее вероятно попали в спам', 'email_send.php', '', 1, 1, 30, 0),
(3, 1438484391, 'Подведение итогов статистики', 'Удаление контакты, помеченные на удаление более месяца назад', 'kont_dell.php', '', 1, 1, 2592000, 4),
(4, 1419051218, 'Очистка временной папки', 'Очистка временной папки', 'clear_tmp_dir.php', '', 1, 1, 86400, 31),
(5, 1419051226, 'Очистка папки с кэшем файлов get_user', 'Очистка папки с кэшем файлов get_user', 'get_user.php', '', 1, 1, 86400, 31),
(6, 1421556877, 'Отчистка системного журнала', 'Удаление корзины  оповещений которым больше чем 30 дней и помечены проверенными ', 'system_jurnal.php', '', 1, 1, 2592000, 3),
(7, 1419051279, 'Незначительные задачи', 'Мелкие задачи которым не нужен отдельный запуск индивидуально\nТакие как чистка неактивированных аккаунтов  или чистка ненужных таблиц гостей .\nПроцесс желательно не останавливать ', 'other.php', '', 1, 0, 86400, 31),
(8, 1419051279, 'Очистка  кэша кабинета и натроек', 'Очистка  кэша кабинета и натроек', 'user_cab_del.php', '', 1, 1, 86400, 31);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
